"""
Copyright (C) 2024 eiraniko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import threading
from .similarity import Similarity
from .item_wrap import ItemWrapObjectDetector
from .tip_img import TipImgObjectDetector


class LazyInstance:
    """
    懒加载包装器：首次调用 get_instance() 时创建实例，
    每次调用时重置超时定时器，如果超过 timeout 秒未被使用，则自动释放实例。
    """

    def __init__(self, create_func, timeout=600):
        self.create_func = create_func
        self.timeout = timeout
        self.instance = None
        self.lock = threading.Lock()
        self.timer = None

    def _release_instance(self):
        with self.lock:
            self.instance = None
            self.timer = None

    def _reset_timer(self):
        # 每次调用时取消已有定时器，重置超时定时器
        if self.timer:
            self.timer.cancel()
        self.timer = threading.Timer(self.timeout, self._release_instance)
        self.timer.daemon = True
        self.timer.start()

    def get_instance(self):
        with self.lock:
            if self.instance is None:
                self.instance = self.create_func()
            self._reset_timer()
            return self.instance


# 使用 LazyInstance 包装各模型的创建，默认超时时间设置为600秒（10分钟）
_sim_nine_lazy = LazyInstance(lambda: Similarity(nine=True), timeout=600)
_sim_default_lazy = LazyInstance(lambda: Similarity(), timeout=600)
_item_detector_lazy = LazyInstance(lambda: ItemWrapObjectDetector(), timeout=600)
_tip_detector_lazy = LazyInstance(lambda: TipImgObjectDetector(), timeout=600)


def compute_similarity_nine(image_1: str, image_2: str):
    instance = _sim_nine_lazy.get_instance()
    return instance.compute_similarity(image_1, image_2)


def compute_similarity(image_1: str, image_2: str):
    instance = _sim_default_lazy.get_instance()
    return instance.compute_similarity(image_1, image_2)


def detection_item(image: str):
    instance = _item_detector_lazy.get_instance()
    return instance.detect(image)


def detection_tip(image: str):
    instance = _tip_detector_lazy.get_instance()
    return instance.detect(image)
