"""
Copyright (c) 2020 Bubbliiiing

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Copyright (C) 2024 eiraniko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import onnxruntime
import numpy
from PIL import Image


class Similarity(object):
    _defaults = {
        # 使用自己训练好的模型进行预测时请修改 model_path
        "model_path": os.path.join(os.path.dirname(__file__), "similarity.onnx"),
        # 输入图片的大小，格式为 [height, width]
        "input_shape": [105, 105],
        # 是否使用 letterbox 方式调整图片（不失真）
        "letterbox_image": True,
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls._defaults[n]
        return "Unrecognized attribute name '" + n + "'"

    def __init__(self, nine=False, **kwargs):
        self.__dict__.update(self._defaults)
        for k, v in kwargs.items():
            setattr(self, k, v)
        if nine:
            self.model_path = os.path.join(
                os.path.dirname(__file__), "similarity_nine.onnx"
            )
        self._load_model()

    def _load_model(self):
        print("Loading ONNX model...")
        self.session = onnxruntime.InferenceSession(self.model_path)
        print("{} model loaded.".format(self.model_path))

    def _prepare_image(self, image, target_size):
        """
        将输入图像转换为 RGB，并调整尺寸以适应模型输入。
        支持 letterbox（不失真填充）和非 letterbox（先 resize 后中心裁剪）两种模式。
        target_size 格式为 (width, height)
        """
        if not isinstance(image, Image.Image):
            image = Image.fromarray(image)
        image = image.convert("RGB")
        target_w, target_h = target_size

        if self.letterbox_image:
            iw, ih = image.size
            scale = min(target_w / iw, target_h / ih)
            nw = int(iw * scale)
            nh = int(ih * scale)
            image_resized = image.resize((nw, nh), Image.BICUBIC)
            new_image = Image.new("RGB", (target_w, target_h), (128, 128, 128))
            new_image.paste(image_resized, ((target_w - nw) // 2, (target_h - nh) // 2))
            return new_image
        else:
            iw, ih = image.size
            # 如果目标尺寸为正方形，采用按较小边匹配的方法
            if target_w == target_h:
                if (iw <= ih and iw == target_w) or (ih <= iw and ih == target_w):
                    resized = image
                else:
                    if iw < ih:
                        new_w = target_w
                        new_h = int(target_w * ih / iw)
                    else:
                        new_h = target_w
                        new_w = int(target_w * iw / ih)
                    resized = image.resize((new_w, new_h), Image.BILINEAR)
            else:
                # 非正方形直接 resize 到目标尺寸
                resized = image.resize((target_w, target_h), Image.BILINEAR)
            # 中心裁剪
            resized_w, resized_h = resized.size
            left = max(0, int(round((resized_w - target_w) / 2.0)))
            top = max(0, int(round((resized_h - target_h) / 2.0)))
            return resized.crop((left, top, left + target_w, top + target_h))

    def _preprocess_input(self, x):
        """
        归一化图像数据至 [0,1]。
        """
        return x.astype(numpy.float32) / 255.0

    def compute_similarity(self, image1_path, image2_path):
        """
        计算两张图片的相似度。
        输入参数为两张图片的路径，输出不变，返回模型推理得到的相似度数值。
        """
        image1 = Image.open(image1_path)
        image2 = Image.open(image2_path)

        # 预处理：转换为RGB、调整尺寸
        target_size = (self.input_shape[1], self.input_shape[0])  # (width, height)
        image1 = self._prepare_image(image1, target_size)
        image2 = self._prepare_image(image2, target_size)

        # 转换为数组、归一化、调整维度
        arr1 = self._preprocess_input(numpy.array(image1, numpy.float32))
        arr2 = self._preprocess_input(numpy.array(image2, numpy.float32))
        arr1 = numpy.expand_dims(numpy.transpose(arr1, (2, 0, 1)), 0)
        arr2 = numpy.expand_dims(numpy.transpose(arr2, (2, 0, 1)), 0)

        # 模型推理
        inputs = {
            self.session.get_inputs()[0].name: arr1,
            self.session.get_inputs()[1].name: arr2,
        }
        output = self.session.run(None, inputs)[0]
        return output[0][0]
