"""
Copyright (C) 2024 eiraniko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import cv2
import numpy
import onnxruntime


class ItemWrapObjectDetector:
    def __init__(self, model_path=None):
        """
        初始化时加载 ONNX 模型，默认模型文件为当前脚本同目录下的 "item_wrap.onnx"。
        """
        if model_path is None:
            model_path = os.path.join(os.path.dirname(__file__), "item_wrap.onnx")
        self.session = onnxruntime.InferenceSession(
            model_path, providers=["CPUExecutionProvider"]
        )
        self.input_name = self.session.get_inputs()[0].name
        self.output_name = self.session.get_outputs()[0].name

    def detect(self, image_path):
        """
        对给定路径的图片进行目标检测，返回检测后的图片及检测框列表。

        参数:
            image_path: 图片文件路径（支持常见格式，如 jpg, png 等）

        返回:
            result_img: 在原始图片上绘制检测框后的图像
            result_boxes: 检测框列表，每个元素格式为 (x1, y1, x2, y2, score)
        """
        # 加载图片（支持常见格式）
        img = cv2.imread(image_path, cv2.IMREAD_COLOR)
        if img is None:
            raise ValueError(f"无法加载图片，请检查路径: {image_path}")

        # 预处理：BGR转RGB，记录原始尺寸，调整尺寸至640x640
        img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        ori_h, ori_w = img_rgb.shape[:2]
        resized_img = cv2.resize(img_rgb, (640, 640), interpolation=cv2.INTER_LINEAR)

        # 图像归一化及维度变换：从 (H, W, C) -> (1, C, H, W)
        inputs = resized_img.astype(numpy.float32) / 255.0
        inputs = numpy.transpose(inputs, (2, 0, 1))[numpy.newaxis, ...]

        # 模型推理
        output = self.session.run([self.output_name], {self.input_name: inputs})[0]
        pred = numpy.transpose(output[0], (1, 0))
        bboxes = pred[:, :4].copy()  # 复制防止后续修改影响原始数据
        scores = numpy.max(pred[:, 4:], axis=1)

        # 将 bbox 中心点表示转换为左上角表示 (x1, y1, w, h)
        bboxes[:, 0] = bboxes[:, 0] - bboxes[:, 2] / 2
        bboxes[:, 1] = bboxes[:, 1] - bboxes[:, 3] / 2

        # 使用 NMS 过滤检测框
        conf_thres = 0.25
        iou_thres = 0.45
        indices = cv2.dnn.NMSBoxes(
            bboxes.tolist(), scores.tolist(), conf_thres, iou_thres
        )
        if len(indices) > 0:
            indices = numpy.array(indices).flatten()
        else:
            indices = []

        # 绘制检测结果并映射回原始图片尺寸
        result_img = img.copy()
        result_boxes = []
        for i in indices:
            bbox = bboxes[i]
            score = scores[i]
            label = f"{score:.2f}"
            x1 = int(bbox[0] * ori_w / 640)
            y1 = int(bbox[1] * ori_h / 640)
            x2 = int((bbox[0] + bbox[2]) * ori_w / 640)
            y2 = int((bbox[1] + bbox[3]) * ori_h / 640)
            cv2.rectangle(result_img, (x1, y1), (x2, y2), (0, 255, 0), 2)
            cv2.putText(
                result_img,
                label,
                (x1, y1),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                (0, 0, 255),
                1,
            )
            result_boxes.append((x1, y1, x2, y2, score))

        return result_img, result_boxes
