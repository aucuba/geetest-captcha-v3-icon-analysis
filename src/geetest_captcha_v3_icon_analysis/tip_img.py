"""
Copyright (C) 2024 eiraniko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import cv2
import numpy
import onnxruntime


class TipImgObjectDetector:
    def __init__(self, model_path=None, target_size=640):
        """
        初始化对象检测器。
        :param model_path: ONNX 模型路径，默认使用与当前文件同目录下的 "tip_img.onnx"
        :param target_size: 模型输入尺寸，默认为 640
        """
        if model_path is None:
            model_path = os.path.join(os.path.dirname(__file__), "tip_img.onnx")
        self.model_path = model_path
        self.target_size = target_size
        self.session = onnxruntime.InferenceSession(
            self.model_path, providers=["CPUExecutionProvider"]
        )
        self.input_name = self.session.get_inputs()[0].name
        self.output_name = self.session.get_outputs()[0].name

    def detect(self, image_path):
        """
        对给定图片路径进行目标检测，返回标注后的图片、预处理后的图片及检测框。
        :param image_path: 图片路径（支持常见图片格式）
        :return: (mark_img, padded_img, result_boxes)
                 mark_img: 在 padded_img 上绘制检测框及得分后的图片
                 padded_img: 预处理后的 RGB 图片
                 result_boxes: 检测框列表，每个元素为 (x1, y1, x2, y2, score)
        """
        # 加载图片（支持常见图片格式）
        img = cv2.imread(image_path)
        if img is None:
            raise ValueError(f"无法加载图片：{image_path}")
        # 如果图片包含 alpha 通道，则转换为 BGR
        if img.shape[2] == 4:
            img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)

        # 预处理：调整长宽比并填充至固定尺寸，再转换为 RGB 格式
        old_size = img.shape[:2]  # (height, width)
        ratio = float(self.target_size) / max(old_size)
        new_size = (int(old_size[0] * ratio), int(old_size[1] * ratio))
        resized_img = cv2.resize(img, (new_size[1], new_size[0]))
        delta_w = self.target_size - new_size[1]
        delta_h = self.target_size - new_size[0]
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        padded_img = cv2.copyMakeBorder(
            resized_img,
            top,
            bottom,
            left,
            right,
            cv2.BORDER_CONSTANT,
            value=[255, 255, 255],
        )
        padded_img = cv2.cvtColor(padded_img, cv2.COLOR_BGR2RGB)

        # 数据归一化及维度调整
        inputs = padded_img / 255.0
        inputs = inputs.transpose((2, 0, 1))[numpy.newaxis, ...].astype(numpy.float32)

        # 模型推理
        output = self.session.run([self.output_name], {self.input_name: inputs})[0]
        pred = numpy.transpose(output[0], (1, 0))
        bboxes = pred[:, :4]
        scores = numpy.max(pred[:, 4:], axis=1)
        # 将中心坐标转换为左上角坐标
        bboxes[:, 0] -= bboxes[:, 2] / 2
        bboxes[:, 1] -= bboxes[:, 3] / 2

        # 非极大值抑制
        conf_thres = 0.25
        iou_thres = 0.45
        indices = cv2.dnn.NMSBoxes(
            bboxes.tolist(), scores.tolist(), conf_thres, iou_thres
        )
        indices = numpy.array(indices).flatten() if len(indices) > 0 else []

        # 绘制检测框和构造检测结果
        result_boxes = []
        mark_img = padded_img.copy()
        for idx in indices:
            bbox = bboxes[idx]
            score = scores[idx]
            label = f"{score:.2f}"
            x1, y1, w, h = map(int, bbox)
            x2, y2 = x1 + w, y1 + h
            cv2.rectangle(mark_img, (x1, y1), (x2, y2), (0, 255, 0), 2)
            cv2.putText(
                mark_img, label, (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1
            )
            result_boxes.append((x1, y1, x2, y2, score))

        return mark_img, padded_img, result_boxes
